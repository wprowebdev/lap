FROM php:7.2-apache

# Install additional packages
RUN apt-get update \
    && apt-get install zlib1g-dev \
    && rm -rf /var/lib/apt/lists/* \
    && docker-php-ext-install mysqli zip

# Allow apache to edit the site by assiging www-data the same id as the mounted volume
RUN groupmod -g 1000 www-data \ 
    && usermod -u 1000 www-data 

